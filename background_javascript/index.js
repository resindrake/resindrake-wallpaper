var blurO = 0;

var customText = "rgba(255, 255, 255, 1)";
var textOpacity = 1;
var customBack = "rgba(200,200,200,.2)";
var backOpacity = 0.2;
var displayDate = true;
var displayTime = true;
var colorBall


window.wallpaperPropertyListener = {
	applyUserProperties: function(properties) {
	
// Clock and Date
	function DigitalClock() {
		var curTime = new Date();
		var Hours24 = curTime.getHours()
		var Hours = curTime.getHours();
		var Minutes = curTime.getMinutes();
		var Seconds = curTime.getSeconds();
		var Milliseconds = curTime.getMilliseconds();
		var mid = 'pm';

		if (Minutes < 10) {
      	Minutes = "0" + Minutes;
    	}	
    	if (Hours>12){
			Hours = Hours - 12;
		}else if (Hours===0){
			Hours = 12;	
		}
    	if (Hours24 < 10) {
      	Hours24 = "0" + Hours24;
    	}
    	if (Seconds < 10) {
      	Seconds = "0" + Seconds;
    	}
    	if(Hours24 < 12) {
       	mid = 'am';
    	}		


		$("#background_24hour").html(Hours24 +":");
		$("#background_12hour").html(Hours +":");
		$("#background_minutes").html(Minutes);
		$("#background_seconds").html(":"+ Seconds);
		$("#background_am_pm").html(mid);
		$("#background_milliseconds").html(Milliseconds);
		$("#background_day").html(curTime.getDay());
		$("#background_day_name").html(weekdays[curTime.getDay()]);
		$("#background_day_name_short").html(weekday[curTime.getDay()]);
		$("#background_day_number").html(curTime.getDate());
		$("#background_month").html(curTime.getMonth()+1);
		$("#background_month_name").html(months[curTime.getMonth()]);
		$("#background_month_name_short").html(month[curTime.getMonth()]);
		$("#background_year").html(curTime.getFullYear().toString().substr(-2));
		$("#background_fullyear").html(curTime.getFullYear());
	}
	setInterval(DigitalClock, 1000/30);



	if(properties.background_months){
			months = properties.background_months.value;
		
			switch (properties.background_months.value){
				case 1:
				months = ["January","February","March","April","May","June","July","August","September","October","November","December"],month = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"],weekdays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],weekday = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
				break;
				case 2:
				months = ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"],month = [/*"Янв","Фев","Мар","Апр","Май","Июн","Июл","Авг","Сен","Окт","Ноя","Дек"*/] ,weekdays = ["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"], weekday = [/*"Вс","Пн","Вт","Ср","Чт","Пт","Сб"*/];
				break;
				case 3:
				months = ["Січень","Лютий","Березень","Квітень","Травень","Червень","Липень","Серпень","Вересень","Жовтень","Листопад","Грудень"],month = [/*"Січ","Лют","Берез","Квіт","Трав","Черв","Лип","Серп","Верес","Жовт","Листоп","Груд"*/] ,weekdays = ["Неділя","Понеділок","Вівторок","Середа","Четвер","П'ятниця","Субота"], weekday = [/*"Нд","Пн","Вт","Ср","Чт","Пт","Сб",*/];
				break;
				case 4:
				months = ["Styczeń","Luty","Marzec","Kwiecień","Maj","Czerwiec","Lipiec","Sierpień","Wrzesień","Październik","Listopad","Grudzień"],month = [] ,weekdays = ["Niedziela","Poniedziałek","Wtorek","Środa","Czwartek","Piątek","Sobota"], weekday = [/*"Ni","Pn","Wt","Śr","Cz","Pt","So"*/];
				break;
				case 5:
				months = ["Gennaio","Febbraio","Marzo","Aprile","Maggio","Giugno","Luglio","Agosto","Settembre","Ottobre","Novembre","Dicembre"],month = [] ,weekdays = ["Domenica","Lunedi","Martedì","Mercoledì","Giovedi","Venerdì","Sabato"], weekday = [];
				break;
				case 6:
				months = ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],month = [] ,weekdays = ["Domingo","Segunda-feira","Terça-feira","Quarta-feira","Quinta-feira","Sexta-feira","Sábado"], weekday = [];
				break;
				case 7:
				months = ["Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"],month = [] ,weekdays = ["Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi"], weekday = [];
				break;
				case 8:
				months = ["Januar","Februar","März","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember"],month = [] ,weekdays = ["Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag"], weekday = [];
				break;
				}
		}

	if(properties.background_Time){
		Time = properties.background_Time.value;
		if(Time === false){
			$('#background_24hour').css("display","none");
			$('#background_am_pm').css("display","block");
			$('#background_12hour').css("display","inline-block");
		}else{
			$('#background_12hour').css("display","none");
			$('#background_am_pm').css("display","none");
			$('#background_24hour').css("display","inline-block");
		}
	}
	if(properties.background_Second){
		Second = properties.background_Second.value;
		if(Second){
			$('#background_seconds').css("display","none");
		}else{
			$('#background_seconds').css("display","inline-block");
		}
	}
	if(properties.background_millisecond){
		millisecond = properties.background_millisecond.value;
		if(millisecond){
			$('#background_milliseconds').css("display","none");
		}else{
			$('#background_milliseconds').css("display","block");
		}
	}
	if(properties.background_clockX){
		ClockValueX = properties.background_clockX.value;
		document.querySelector("#background_clock").style.left = ClockValueX+"%";
	}
	if(properties.background_clockY){
		ClockValueY = properties.background_clockY.value;
		document.querySelector("#background_clock").style.top = ClockValueY+"%";
	}
	if(properties.background_displaytime){
		displaytime = properties.background_displaytime.value;
		if(displaytime){
			document.querySelector("#background_clock").style.display = "inline-flex";
		}else{
			document.querySelector("#background_clock").style.display = "none";
		}
	}
	if(properties.background_clockcolour){
		customClock = properties.background_clockcolour.value.split(' ');
		customClock = customClock.map(function(c) {
			return Math.ceil(c * 255);
			});
		clockColour ="rgba(" + customClock + ")";
		document.querySelector("#background_clock").style.color = clockColour;
	}
	if(properties.background_clockopacity){
		clockOpacity = properties.background_clockopacity.value / 100;
		clockColour ="rgba(" + customClock + "," + clockOpacity + ")";
		document.querySelector("#background_clock").style.color = clockColour;
	}
	if(properties.background_clocksize){
		ClockSize = properties.background_clocksize.value;
		document.querySelector("#background_clock").style.fontSize = ClockSize+"px";
		document.querySelector(".background_clock_details").style.fontSize = (ClockSize/2)+"px";
	}
	if(properties.background_clockR){
		clockRotate = properties.background_clockR.value;
		document.querySelector("#background_clock").style.transform = 'rotate('+clockRotate+'deg)'; 
	}
	if(properties.background_clockWeight){
		clockWeight = properties.background_clockWeight.value;
		if(clockWeight){
			document.querySelector("#background_clock").style.fontWeight = 800;
		}else{
			document.querySelector("#background_clock").style.fontWeight = 400;
		}
		
	}
	if(properties.background_fonttime){
		if(properties.background_fonttime.value) {
			switch (properties.background_fonttime.value){
				case 1:
				fontTime = "'Orbitron-Regular'";
				break;
				case 2:
				fontTime = "'Nunito-Italic'";
				break;
				case 3:
				fontTime = "'Nunito-Regular'";
				break;
				case 4:
				fontTime = "'asinastra'";
				break;
				case 5:
				fontTime = "'VT323'";
				break;
				case 6:
				fontTime = "'Playball'";
				break;
				case 7:
				fontTime = "'knewave'";
				break;
				case 8:
				fontTime = "'knewave-outline'";
				break;
				case 9:
				fontTime = "'Miltown'";
				break;
				case 10:
				fontTime = "'RalewayDots'";
				break;
				case 11:
				fontTime = "'VastShadow'";
				break;
				case 12:
				fontTime = "'RobotoSlab'";
				break;
				case 13:
				fontTime = "'ReenieBeanie'";
				break;
				case 14:
				fontTime = "'LoveYaLikeASister'";
				break;
				case 15:
				fontTime = "'Lobster'";
				break;
				case 16:
				fontTime = "'Ubuntu'";
				break;
				case 17:
				fontTime = "'BrushieBrushie'";
				break;
				case 18:
				fontTime = "'Zamenhof-inverse'";
				break;
				case 19:
				fontTime = "'Zamenhof-outline'";
				break;
				case 20:
				fontTime = "'Astakhov-Dished-B-Serif'";
				break;
				case 21:
				fontTime = "'Astakhov-Dished-B2-Serif'";
				break;
				case 22:
				fontTime = "'Capture-it-2'";
				break;
			}
		}
		document.querySelector("#background_clock").style.fontFamily = fontTime;
	}

	if(properties.background_displaydate){
		displayDate = properties.background_displaydate.value;
		if(displayDate){
			document.querySelector("#background_date").style.display = "inline-flex";
		}else{
			document.querySelector("#background_date").style.display = "none";
		}
	}
	if(properties.background_datecolour){
		customDate = properties.background_datecolour.value.split(' ');
		customDate = customDate.map(function(c) {
			return Math.ceil(c * 255);
			});
		dateColor ="rgba(" + customDate + ")";
		document.querySelector("#background_date").style.color = dateColor;
	}
	if(properties.background_dateopacity){
		dateOpacity = properties.background_dateopacity.value / 100;
		dateColour ="rgba(" + customDate + "," + dateOpacity + ")";
		document.querySelector("#background_date").style.color = dateColour;
	}
	if(properties.background_datesize){
		DateSize = properties.background_datesize.value;
		document.querySelector("#background_date").style.fontSize = DateSize+"px";
	}
	if(properties.background_dateX){
		DateValueX = properties.background_dateX.value;
		document.querySelector("#background_date").style.left = DateValueX+"%";
	}
	if(properties.background_dateY){
		DateValueY = properties.background_dateY.value;
		document.querySelector("#background_date").style.top = DateValueY+"%";
	}
	if(properties.background_dateR){
		dateRotate = properties.background_dateR.value;
		document.querySelector("#background_date").style.transform = 'rotate('+dateRotate+'deg)'; 
	}
	if(properties.background_dateblock1){
		if(properties.background_dateblock1.value) {
			switch (properties.background_dateblock1.value){
				case 1:
				$('#background_date_block_one').html('<span style="display: none;"></span>');
				break;
				case 2:
				$('#background_date_block_one').html('<span id="day_name">loading...</span>');
				break;
				case 3:
				$('#background_date_block_one').html('<span id="day_name_short">loading...</span>');
				break;
			}
		}
	}
	if(properties.background_punctuationmark1){
		if(properties.background_punctuationmark1.value) {
			switch (properties.background_punctuationmark1.value){
				case 1:
				$('#background_punctuation_mark_one').html('<span style="display: none;"></span>');
				break;
				case 2:
				$('#background_punctuation_mark_one').html('<span style="padding: 0;">.</span>');
				break;
				case 3:
				$('#background_punctuation_mark_one').html('<span style="padding: 0;">,</span>');
				break;
				case 4:
				$('#background_punctuation_mark_one').html('<span style="padding: 0 0 0 0.25em;">/</span>');
				break;
				case 5:
				$('#background_punctuation_mark_one').html('<span style="padding: 0 0 0 0.25em;">-</span>');
				break;
			}
		}
	}
	if(properties.background_dateblock2){
		if(properties.background_dateblock2.value) {
			switch (properties.background_dateblock2.value){
				case 1:
				$('#background_date_block_two').html('<span style="display: none;"></span>');
				break;
				case 2:
				$('#background_date_block_two').html('<span style="padding: 0 0 0 0.25em;" id="month_name">loading...</span>');
				break;
				case 3:
				$('#background_date_block_two').html('<span style="padding: 0 0 0 0.25em;" id="month_name_short">loading...</span>');
				break;
				case 4:
				$('#background_date_block_two').html('<span style="padding: 0 0 0 0.25em;" id="month">loading...</span>');
				break;
				case 5:
				$('#background_date_block_two').html('<span style="padding: 0 0 0 0.25em;" id="day_name">loading...</span>');
				break;
				case 6:
				$('#background_date_block_two').html('<span style="padding: 0 0 0 0.25em;" id="day_name_short">loading...</span>');
				break;
				case 7:
				$('#background_date_block_two').html('<span style="padding: 0 0 0 0.25em;" id="day_number">loading...</span>');
				break;
				case 8:
				$('#background_date_block_two').html('<span style="padding: 0 0 0 0.25em;" id="fullyear">loading...</span>');
				break;
				case 9:
				$('#background_date_block_two').html('<span style="padding: 0 0 0 0.25em;" id="year">loading...</span>');
				break;					
			}
		}
	}
	if(properties.background_punctuationmark2){
		if(properties.background_punctuationmark2.value) {
			switch (properties.background_punctuationmark2.value){
				case 1:
				$('#background_punctuation_mark_two').html('<span style="display: none;"></span>');
				break;
				case 2:
				$('#background_punctuation_mark_two').html('<span style="padding: 0">.</span>');
				break;
				case 3:
				$('#background_punctuation_mark_two').html('<span style="padding: 0">,</span>');
				break;
				case 4:
				$('#background_punctuation_mark_two').html('<span style="padding: 0 0 0 0.25em;">/</span>');
				break;
				case 5:
				$('#background_punctuation_mark_two').html('<span style="padding: 0 0 0 0.25em;">-</span>');
				break;
			}
		}
	}
	if(properties.background_dateblock3){
		if(properties.background_dateblock3.value) {
			switch (properties.background_dateblock3.value){
				case 1:
				$('#background_date_block_three').html('<span style="display: none;"></span>');
				break;
				case 2:
				$('#background_date_block_three').html('<span style="padding: 0 0 0 0.25em;" id="month_name">loading...</span>');
				break;
				case 3:
				$('#background_date_block_three').html('<span style="padding: 0 0 0 0.25em;" id="month_name_short">loading...</span>');
				break;
				case 4:
				$('#background_date_block_three').html('<span style="padding: 0 0 0 0.25em;" id="month">loading...</span>');
				break;
				case 5:
				$('#background_date_block_three').html('<span style="padding: 0 0 0 0.25em;" id="day_name">loading...</span>');
				break;
				case 6:
				$('#background_date_block_three').html('<span style="padding: 0 0 0 0.25em;" id="day_name_short">loading...</span>');
				break;
				case 7:
				$('#background_date_block_three').html('<span style="padding: 0 0 0 0.25em;" id="day_number">loading...</span>');
				break;				
			}
		}
	}
	if(properties.background_punctuationmark3){
		if(properties.background_punctuationmark3.value) {
			switch (properties.background_punctuationmark3.value){
				case 1:
				$('#background_punctuation_mark_three').html('<span style="display: none;"></span>');
				break;
				case 2:
				$('#background_punctuation_mark_three').html('<span style="padding: 0;">.</span>');
				break;
				case 3:
				$('#background_punctuation_mark_three').html('<span style="padding: 0;">,</span>');
				break;
				case 4:
				$('#background_punctuation_mark_three').html('<span style="padding: 0 0 0 0.25em;">/</span>');
				break;
				case 5:
				$('#background_punctuation_mark_three').html('<span style="padding: 0 0 0 0.25em;">-</span>');
				break;
			}
		}
	}
	if(properties.background_dateblock4){
		if(properties.background_dateblock4.value) {
			switch (properties.background_dateblock4.value){
				case 1:
				$('#background_date_block_four').html('<span style="display: none;"></span>');
				break;
				case 2:
				$('#background_date_block_four').html('<span style="padding: 0 0 0 0.25em;" id="day_name">loading...</span>');
				break;
				case 3:
				$('#background_date_block_four').html('<span style="padding: 0 0 0 0.25em;" id="day_name_short">loading...</span>');
				break;
				case 4:
				$('#background_date_block_four').html('<span style="padding: 0 0 0 0.25em;" id="day_number">loading...</span>');
				break;
				case 5:
				$('#background_date_block_four').html('<span style="padding: 0 0 0 0.25em;" id="fullyear">loading...</span>');
				break;
				case 6:
				$('#background_date_block_four').html('<span style="padding: 0 0 0 0.25em;" id="year">loading...</span>');
				break;					
			}
		}
	}
	if(properties.background_fontdate){
		if(properties.background_fontdate.value) {
			switch (properties.background_fontdate.value){
				case 1:
				fontDate = "'Orbitron-Regular'";
				break;
				case 2:
				fontDate = "'Nunito-Italic'";
				break;
				case 3:
				fontDate = "'Nunito-Regular'";
				break;
				case 4:
				fontDate = "'asinastra'";
				break;
				case 5:
				fontDate = "'VT323'";
				break;
				case 6:
				fontDate = "'Playball'";
				break;
				case 7:
				fontDate = "'knewave'";
				break;
				case 8:
				fontDate = "'knewave-outline'";
				break;
				case 9:
				fontDate = "'Miltown'";
				break;
				case 10:
				fontDate = "'RalewayDots'";
				break;
				case 11:
				fontDate = "'VastShadow'";
				break;
				case 12:
				fontDate = "'RobotoSlab'";
				break;
				case 13:
				fontDate = "'ReenieBeanie'";
				break;
				case 14:
				fontDate = "'LoveYaLikeASister'";
				break;
				case 15:
				fontDate = "'Lobster'";
				break;
				case 16:
				fontDate = "'Ubuntu'";
				break;
				case 17:
				fontDate = "'BrushieBrushie'";
				break;
				case 18:
				fontDate = "'Zamenhof-inverse'";
				break;
				case 19:
				fontDate = "'Zamenhof-outline'";
				break;
				case 20:
				fontDate = "'Astakhov-Dished-B-Serif'";
				break;
				case 21:
				fontDate = "'Astakhov-Dished-B2-Serif'";
				break;
				case 22:
				fontDate = "'Capture-it-2'";
				break;
			}
		}
		document.querySelector("#date").style.fontFamily = fontDate;
	}
	if(properties.background_dateWeight){
		dateWeight = properties.background_dateWeight.value;
		if(dateWeight){
			document.querySelector("#date").style.fontWeight = 800;
		}else{
			document.querySelector("#date").style.fontWeight = 400;
		}
	}
// Clock and Date END


	if(properties.background_bluro){
		blurO=properties.background_bluro.value;
		// if (properties.background_bluro.value) {"blur("+blurO+"px)"
			document.querySelector("canvas").style.filter = 'blur('+blurO+'px)';
		// }
	}

	if(properties.background_bgstyles){
		if(properties.background_bgstyles.value) {
			const canvas = document.getElementById('background_canvas');
			const ctx = canvas.getContext('2d')
			switch (properties.background_bgstyles.value){
				case 1:
				ctx.filter = 'contrast(1) sepia(0)';
				//document.querySelector("canvas").style.filter = 'invert(50%) sepia(88%) saturate(2974%) hue-rotate(162deg) brightness(96%) contrast(101%)';
				break;
				case 2:
				ctx.filter = 'contrast(1.4) sepia(1)';
				break;
				case 3:
				ctx.filter = 'hue-rotate(200deg)';
				break;
				case 4:
				ctx.filter = 'hue-rotate(380deg)';
				break;
				case 5:
				ctx.filter = 'hue-rotate(70deg)';
				break;
				case 6:
				ctx.filter = 'hue-rotate(120deg)';
				break;
				case 7:
				ctx.filter = 'hue-rotate(160deg)';
				break;
				case 8:
				ctx.filter = 'hue-rotate(180deg)';
				break;
				case 9:
				ctx.filter = 'hue-rotate(250deg)';
				break;
				case 10:
				ctx.filter = 'hue-rotate(290deg)';
				break;
				case 11:
				ctx.filter = 'hue-rotate(300deg)';
				break;
				case 12:
				ctx.filter = 'hue-rotate(330deg)';
				break;
				case 13:
				ctx.filter = 'hue-rotate(316deg)';
				break;
				case 14:
				ctx.filter = 'contrast(2.4) hue-rotate(200deg)';
				break;
				case 15:
				ctx.filter = 'grayscale(100%)';
				break;
				case 16:
				ctx.filter = 'brightness(300%)';
				break;
			}
		}	
	}



		
	}
}





