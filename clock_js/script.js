"use strict";

const range = (count, first = null) => (a => first === null ? a : a.map(i => first + i))([...Array(count).keys()]);

(function() {
	const rangeTo = 270;
	const sectionsDayName = 7;
	const sectionsDay = 31;
	const sectionsMonth = 12;
	const charactersDayName = 3;
	const charactersDay = 2;
	const charactersMonth = 3;
	const colours = {day: "#FF2D55", month: "#007AFF", dow: "#4CD964"};

	// Rotate the selected ring the correct amount and illuminate the correct characters of the ring text
	const rotateRing = function(input, sections, characters, ring, text, colour) {
		const sectionWidth = rangeTo / sections;
		const initialRotation = 135 - sectionWidth / 2;
		const start = characters * input + input - characters;
		$(ring).css({transform: `rotate(${initialRotation - (input - 1) * sectionWidth}deg)`});
		for (const n of range(characters, start)) $(text).children(".char" + n).css({color: colour});
	};

	// Give column representing passed days and the current day this week a height
	const loadBars = function(dayName) {
		for (const i of range(dayName, 1)) {
			const newHeight = Math.floor(85 * Math.random() + 5);
			const newTop = 110 - newHeight;
			$(`#clock_x${i}`).css({height: `${newHeight}px`});
		}
	};

	const init = function() {
		// Set previews
		$(".clock_center-preview").lettering();
		$(".clock_day-name-preview").lettering();
		$(".clock_day-name-text").lettering();
		$(".clock_day-preview").lettering();
		$(".clock_day-text").lettering();
		$(".clock_month-preview").lettering();
		$(".clock_month-text").lettering();
		$(".clock_day-preview").fadeTo(10, 1);
		$(".clock_month-preview").fadeTo(10, 1);
		$(".clock_day-name-preview").fadeTo(10, 1);
		$(".clock_center-preview").fadeTo(10, 1);

		const date = new Date();

		// Crossfade and animate day dial
		const day = date.getDate(); // 1..31
		setTimeout(function() {
			$(".clock_day-preview").fadeTo(500, 0);
			$(".clock_day-text").fadeTo(500, 1, () => rotateRing(day, sectionsDay, charactersDay, "#clock_r3", ".clock_day-text", colours.day));
		}, 500);

		// Crossfade and animate month dial side elements
		const month = date.getMonth() + 1; // 1..12
		setTimeout(function() {
			$(".clock_month-preview").fadeTo(500, 0);
			$(".clock_fa-cloud").fadeTo(500, 1);
			$(".clock_temperature").fadeTo(500, 1);
			$(".clock_bars").fadeTo(500, 1);
			$(".clock_month-text").fadeTo(500, 1, function() {
				rotateRing(month, sectionsMonth, charactersMonth, "#clock_r2", ".clock_month-text", colours.month);
				loadBars(dayName);
			});
		}, 1000);

		// Crossfade and animate DoW dial
		const dayName = date.getDay() % 7; // 0..6
		setTimeout(function() {
			$(".clock_day-name-preview").fadeTo(500, 0);
			$(".clock_day-name-text").fadeTo(500, 1, () => rotateRing(dayName, sectionsDayName, charactersDayName, "#clock_r1", ".clock_day-name-text", colours.dow));
		}, 1500);

		// Crossfade clock face
		setTimeout(function() {
			$(".clock_center-preview").fadeTo(500, 0);
			$(".clock_head").fadeTo(500, 0);
			$(".clock_torso").fadeTo(500, 0);
			$(".clock_hand-container").fadeTo(500, 1);
		}, 2000);

		// Update second hand every 50 ms
		setInterval(function() {
			const date = new Date();
			document.getElementById("clock_seconds").style.transform = `rotate(${6 * (date.getSeconds() + date.getMilliseconds() / 1000)}deg)`;
		}, 50);

		// Update minute and hour hands once now, and then every 10 s starting from the next multiple of 10
		const updateMinuteHour = function() {
			const date = new Date();
			const minutesPart = date.getMinutes();
			document.getElementById("clock_minutes").style.transform = `rotate(${6 * minutesPart}deg)`;
			document.getElementById("clock_hours").style.transform = `rotate(${30 * date.getHours() + (minutesPart / 2)}deg)`;
		};
		updateMinuteHour();
		setTimeout(() => setInterval(updateMinuteHour, 10000), 10050 - (d => 1000 * d.getSeconds() + d.getMilliseconds())(new Date()));
	};

	init();
})();
