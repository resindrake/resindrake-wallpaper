var canvas = document.getElementById("music_canvas");
var ctx = canvas.getContext("2d");

var bars = [];
var particles = [];
var barsLength = 64;
var peakValue = 1;
var smoothingFactor = 2;
var logo = new Image();
var blackLogo = new Image();
var timeSinceLastPing = Date.now();
var lastCalled = Date.now();
var user = {
    barWidth: 20,
    barPadding: 5,
    header: "Header",
    subheader: "Sub Header",
    enableParticles: true,
    particleLength: 150,
    colour: "#F7218A",
    smoothingFactor: 5,
    equalizeAudio: true,
    fps: 30,
    customLogo: false,
    logoImage: new Image(),
    customBackground: false,
    textColour: "#FFF",
    peak: 1,
    hideBox: false,
    refreshToken: "",
    spotifyToken: "",
    enableSpotify: false,
    titleOrder: true,
    updateRate: 5,
    particleSpeed: 1,
    headerSize: 100,
    subheaderSize: 50

};

//Thanks to Squee
var pinknoise = [1.1760367470305, 0.85207379418243,0.68842437227852,0.63767902570829,0.5452348949654,0.50723325864167,0.4677726234682,0.44204182748767,
    0.41956517802157,0.41517375040002,0.41312118577934,0.40618363960446,0.39913707474975,0.38207008614508,0.38329789106488,0.37472136606245,
    0.36586428412968,0.37603017335105,0.39762590761573,0.39391828858591,0.37930603769622,0.39433365764563,0.38511504613859,0.39082579241834,
    0.3811852720504,0.40231453727161,0.40244151133175,0.39965366884521,0.39761103827545,0.51136400422212,0.66151212038954,0.66312205226679,
    0.7416276690995,0.74614971301133,0.84797007577483,0.8573583910469,0.96382997811663,0.99819377577185,1.0628692615814,1.1059083969751,
    1.1819808497335,1.257092297208,1.3226521464753,1.3735992532905,1.4953223705889,1.5310064942373,1.6193923584808,1.7094805527135,
    1.7706604552218,1.8491987941428,1.9238418849406,2.0141596921333,2.0786429508827,2.1575522518646,2.2196355526005,2.2660112509705,
    2.320762171749,2.3574848254513,2.3986127976537,2.4043566176474,2.4280476777842,2.3917477397336,2.4032522546622,2.3614180150678	];


/*
Spotify Request
*/

var xhr = new XMLHttpRequest();
var request = new XMLHttpRequest();

function processRequest() {
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var response = JSON.parse(xhr.responseText);
            if(response.is_playing) {
                if(response.item.name) {
                    if(user.titleOrder) {
                        if(response.item.name == user.header) {
                            return;
                        }
                        user.header = response.item.name.replace(/ *\([^)]*\) */g, "").split("-")[0];
                    } else {
                        if(response.item.name == user.subheader) {
                            return;
                        }
                        user.subheader = response.item.name.replace(/ *\([^)]*\) */g, "").split("-")[0];
                    }
                }
                if(response.item.artists) {
                    var res = "";
                    for(var i = 0; i < response.item.artists.length; i++) {
                        res += response.item.artists[i].name + (i == response.item.artists.length-1 ? "" : ", ");
                    }
                    if(user.titleOrder) {
                        user.subheader = res;
                    } else {
                        user.header = res
                    }
                }
                if(response.item.album.images) {
                    if(user.spotifyLogo) {
                        user.customLogo = true;
                        user.logoImage.loaded = false;
                        user.logoImage.src = response.item.album.images[0].url;
                        user.logoImage.onload = function() {
                            user.logoImage.loaded = true;
                            user.logo = user.logoImage
                            if(user.syncColour) {
                                var colour = getAverageRGB(user.logo);
                                user.colour = "rgb(" + colour.r + ", " + colour.g + ", " + colour.b + ")";
                            }
                        }
                    }
                }
            } else {
                user.header="Spotify";
                user.subheader="No music is currently playing";
            }
        } else if (xhr.readyState == 4) {
            if(xhr.responseText != "") {
                var response = JSON.parse(xhr.responseText);
                if(response.error.status == 401 && response.error.message == "The access token expired") {
                    user.header = "Loading...";
                    user.subheader = "Loading...";
                    getToken();
                } else {
                    user.header = "Error " + response.error.status;
                    user.subheader = response.error.message;
                }
            }
        }
    };
}

function getAverageRGB(imgEl) {

    var blockSize = 10, // only visit every 5 pixels
        defaultRGB = {r:0,g:0,b:0}, // for non-supporting envs
        canvas = document.createElement('canvas'),
        context = canvas.getContext && canvas.getContext('2d'),
        data, width, height,
        i = -4,
        length,
        rgb = {r:0,g:0,b:0},
        count = 0;

    if (!context) {
        return defaultRGB;
    }

    height = canvas.height = imgEl.naturalHeight || imgEl.offsetHeight || imgEl.height;
    width = canvas.width = imgEl.naturalWidth || imgEl.offsetWidth || imgEl.width;

    context.drawImage(imgEl, 0, 0);

    try {
        data = context.getImageData(0, 0, width, height);
    } catch(e) {
        return defaultRGB;
    }

    length = data.data.length;

    while ( (i += blockSize * 4) < length ) {
        ++count;
        rgb.r += data.data[i];
        rgb.g += data.data[i+1];
        rgb.b += data.data[i+2];
    }

    // ~~ used to floor values
    rgb.r = ~~(rgb.r/count);
    rgb.g = ~~(rgb.g/count);
    rgb.b = ~~(rgb.b/count);

    return rgb;

}

var last = performance.now() / 1000;
var fpsThreshold = 0;

logo.src = "music/logo.png";
blackLogo.src = "music/black_logo.png";
logo.onload = function() {
    logo.loaded = true;
}

blackLogo.onload = function() {
    blackLogo.loaded = true;
}

window.wallpaperPropertyListener = {
    applyUserProperties: function(properties) {
        if(properties.header) {
            user.header = properties.header.value;
        }
        if(properties.subHeader) {
            user.subheader = properties.subHeader.value;
        }
        if(properties.refreshToken) {
            user.refreshToken = properties.refreshToken.value;
            getToken();
        }
        if(properties.enableParticles) {
            user.enableParticles = properties.enableParticles.value;
        }
        if(properties.particleAmount) {
            user.particleLength = properties.particleAmount.value;
            updateParticles(user.particleLength);
        }
        if(properties.colourCombo) {
            user.colour = properties.colourCombo.value;
            if(!user.customLogo) {
                DetectColour();
            }
        }
        if(properties.customText) {
            var colour = properties.customText.value.split(' ');
            for(var i = 0; i < colour.length; i++) {
                colour[i] = (colour[i] * 255).toString(16);
                if(colour[i].length == 1) {
                    colour[i] = "0" + colour[i];
                }
            }
            colour = colour.join("");
            colour = "#" + colour;
            user.textColour = colour;
        }
        if(properties.customColour) {
            user.customColour = properties.customColour.value;
        }
        if(properties.colourPicker) {
            if(user.customColour) {
                var colour = properties.colourPicker.value.split(' ');
                for(var i = 0; i < colour.length; i++) {
                    colour[i] = (colour[i] * 255).toString(16);
                    if(colour[i].length == 1) {
                        colour[i] = "0" + colour[i];
                    }
                }
                colour = colour.join("");
                colour = "#" + colour;
                user.colour = colour;
                if(!user.customLogo) {
                    DetectColour();
                }
            }
        }
        if(properties.customBackground) {
            user.customBackground = properties.customBackground.value;
            if(!user.customBackground) {
                canvas.style.backgroundImage="none";
            } else if(user.backgroundImage) {
                canvas.style.backgroundImage=user.backgroundImage;
            }
        }
        if(properties.backgroundImage) {
            if(user.customBackground || properties.customBackground) {
                user.backgroundImage = "url(file:///" + properties.backgroundImage.value + ")";
                canvas.style.backgroundImage = user.backgroundImage;
            }
        }
        if(properties.smoothingFactor) {
            user.smoothingFactor = properties.smoothingFactor.value;
        }
        if(properties.equalizeAudio) {
            user.equalize = properties.equalizeAudio.value;
        }
        if(properties.barWidth) {
            user.barWidth = properties.barWidth.value;
            UpdateSize();
        }
        if(properties.barPadding) {
            user.barPadding = properties.barPadding.value;
            UpdateSize();
        }
        if(properties.customLogo) {
            user.customLogo = properties.customLogo.value;
            if(!user.customLogo) {
                DetectColour();
            } else if (user.logoImage) {
                user.logo = user.logoImage;
            }
        }
        if(properties.logoImage) {
            user.logoImage.loaded = false;
            user.logoImage.src = "file:///" + properties.logoImage.value;
            user.logoImage.onload = function() {
                user.logoImage.loaded = true;
            }
            if(user.customLogo) {
                user.logo = user.logoImage;
            } else {
                DetectColour();
            }
        }
        if(properties.hideBox) {
            user.hideBox = properties.hideBox.value;
        }
        if(properties.fps) {
            user.fps = properties.fps.value;
        }
        if(properties.peak) {
            user.peak = properties.peak.value;
        }
        if(properties.headerSize) {
            user.headerSize = properties.headerSize.value;
        }
        if(properties.subheaderSize) {
            user.subheaderSize = properties.subheaderSize.value;
        }
        if(properties.spotifyLogo) {
            user.spotifyLogo = properties.spotifyLogo.value;
        }
        if(properties.syncColour) {
            user.syncColour = properties.syncColour.value;
            if(user.syncColour && user.spotifyLogo) {
                var colour = getAverageRGB(user.logo);
                user.colour = "rgb(" + colour.r + ", " + colour.g + ", " + colour.b + ")";
            }
        }
        if(properties.enableSpotify) {
            user.enableSpotify = properties.enableSpotify.value;
        }
        if(properties.titleOrder) {
            user.titleOrder = properties.titleOrder.value;
        }
        if(properties.updateRate) {
            user.updateRate = properties.updateRate.value;
        }
        if(properties.particleSpeed) {
            user.particleSpeed = properties.particleSpeed.value;
        }
    }
};

window.wallpaperRegisterAudioListener(function(audioData) {
    var max = 0;
    var data = [];
    for( i = 0; i < 64; i++ ) {
        data.push(Math.pow(audioData[i]+audioData[i+64],user.peak)/2)
        if(i == 0){
        }
        if(user.equalize) {
            data[i] /= pinknoise[i];
        }
        if(data[i] > max) {
            max = data[i];
        }
    }

    peakValue = peakValue * 0.98 + max * 0.02;
    for(i = 0; i < data.length; i++ ) {
        data[i] /= peakValue;
        if(data[i] > 1.2) {
            data[i] = 1.2;
        }
    }
    var newAudio = [];
    var average = 0;
    for(var i = 0; i < data.length; i++) {
        if(i == 0 || i == data.length-1) {
            newAudio[i] = data[i];
        } else {
            newAudio[i] = (data[i-1]*2+data[i]*3+data[i+1]*2)/7;
        }
        average+=newAudio[i];
    }
    average /= 64;
    average *= user.peak*user.particleSpeed
    for(var i = 0; i < particles.length; i++) {
        particles[i].changedSpeedX = particles[i].speedX*(average+0.1);
        particles[i].changedSpeedY = particles[i].speedY*(average+0.1);
    }
    for(var i = 0; i < bars.length; i++) {
        bars[i].desiredPos = newAudio[i]*canvas.height/3+1;
    }
});

function getToken() {
    if(user.refreshToken.length != 134) {
        return;
    }
    if(Date.now()-lastCalled > 2000) {
        lastCalled = Date.now();
        request.open('GET', "http://audiovisualiser.herokuapp.com/refresh?refresh_token=" + user.refreshToken, true);
        request.send();
        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
                var response = JSON.parse(request.responseText);
                if(response.access_token) {
                    user.spotifyToken=response.access_token;
                }
            }
        };
    }
}

function DetectColour() {
    if(!user.colour) {
        user.colour = "#FFF";
    }
    var c = user.colour.toString().substring(1);
    var rgb = parseInt(c, 16);
    var r = (rgb >> 16) & 0xff;
    var g = (rgb >>  8) & 0xff;
    var b = (rgb >>  0) & 0xff;

    var brightness = Math.round((r*299+g*587+b*114) / 1000);
    if (brightness > 240) {
        user.logo = blackLogo;
    } else {
        user.logo = logo;
    }
}

function UpdateSize() {
    for(var i = 0; i < bars.length; i++) {
        bars[i].x = canvas.width/2-(barsLength*(user.barWidth+user.barPadding))/2+i*(user.barWidth+user.barPadding)
        bars[i].y = canvas.height/2-25
        bars[i].width = user.barWidth
        bars[i].height = 25
    }
}

function updateParticles(size) {
    if(size > particles.length) {
        while(particles.length < size) {
            particles.push(new Particle(Math.random()*canvas.width,Math.random()*canvas.height,Math.random()*1.5,Math.random()*0.5+0.5,Math.random()*5,Math.random()*2));
        }
    } else {
        var diff = particles.length-size;
        particles.splice(-diff,diff);
    }
}

function resize() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    canvas.style.width = window.innerWidth + "px";
    canvas.style.height = window.innerHeight + "px";
}

window.addEventListener("resize", resize);

function Bar(x,y,width,height,colour) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.desiredPos = this.height;
    this.draw = function() {
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }
    this.update = function() {
        if(this.height < this.desiredPos) {
            this.height+=(this.desiredPos-this.height)/user.smoothingFactor;
        } else if(this.height > this.desiredPos) {
            this.height-=(this.height-this.desiredPos)/user.smoothingFactor;
        }
        this.y = canvas.height/2-this.height;
    }
}

function Particle(x,y,radius,opacity, speedX, speedY) {
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.opacity = opacity;
    this.speedX = speedX;
    this.speedY = speedY;
    this.changedSpeedX = speedX;
    this.changedSpeedY = speedY;
    this.draw = function() {
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2);
        ctx.fillStyle="rgba(255,255,255," + this.opacity + ")";
        ctx.fill();
    }
    this.update = function() {
        this.x+=this.changedSpeedX;
        this.y+=this.changedSpeedY;
        if(this.x > canvas.width) {
            this.x = 0;
        } else if (this.x < 0) {
            this.x = canvas.width;
        }
        if(this.y > canvas.height) {
            this.y = 0;
        } else if (this.y < 0) {
            this.y = canvas.height;
        }
    }
}

window.onload = function() {
    resize();
    for(var i = 0; i < barsLength; i++) {
        bars.push(new Bar(canvas.width/2-(barsLength*(user.barWidth+user.barPadding))/2+i*(user.barWidth+user.barPadding),canvas.height/2-25,user.barWidth,25))
    }
    for(var i = 0; i < user.particleLength; i++) {
        particles.push(new Particle(Math.random()*canvas.width,Math.random()*canvas.height,Math.random()*1.5,Math.random()*0.5+0.5,Math.random()*5,Math.random()*4-2));
    }
    window.requestAnimationFrame(draw);
}

function draw() {
    window.requestAnimationFrame(draw);
    var now = performance.now() / 1000;
    var dt = Math.min(now - last, 1);
    last = now;

    if (user.fps > 0) {
        fpsThreshold += dt;
        if (fpsThreshold < 1.0 / user.fps) {
            return;
        }
        fpsThreshold -= 1.0 / user.fps;
    }

    if(user.enableSpotify) {
        if(user.spotifyToken == "" || user.spotifyToken == undefined) {
            getToken();
            user.header = "Token not found";
            user.subheader = "Please fill in the token parameter"
        } else if(Date.now()-timeSinceLastPing > user.updateRate*1000) {
            timeSinceLastPing = Date.now();
            xhr.open('GET', "https://api.spotify.com/v1/me/player/currently-playing", true);
            xhr.setRequestHeader('Accept', 'application/json');
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.setRequestHeader('Authorization', 'Bearer ' + user.spotifyToken);
            xhr.send();
            processRequest();
        }
    }

    ctx.clearRect(0,0,canvas.width, canvas.height);
    if(user.enableParticles) {
        for(var i = 0; i < particles.length; i++) {
            particles[i].draw();
            particles[i].update();
        }
    }
    ctx.fillStyle=user.colour;
    for(var i = 0; i < bars.length; i++) {
        bars[i].draw();
        bars[i].update();
    }
    if(user.logo) {
        if(!user.hideBox) {
            ctx.fillRect(canvas.width/2-(barsLength*(user.barWidth+user.barPadding))/2,canvas.height/2+30,190,190);
        }
        if(user.logo.loaded) {
            ctx.drawImage(user.logo,canvas.width/2-(barsLength*(user.barWidth+user.barPadding))/2+(user.customLogo ? 20 : 0),canvas.height/2+(user.customLogo ? 50 : 30),(user.customLogo ? 150 : 190),(user.customLogo ? 150 : 190))
        }
    }
    ctx.font = user.headerSize + "px Gotham-Bold";
    ctx.textBaseline = 'muddle';
    ctx.fillStyle=user.textColour;
    ctx.fillText(user.header.toUpperCase(),canvas.width/2-(barsLength*(user.barWidth+user.barPadding))/2+218,canvas.height/2+40+user.headerSize)
    ctx.font = user.subheaderSize + 'px Gotham-Light';
    ctx.fillText(user.subheader.toUpperCase(),canvas.width/2-(barsLength*(user.barWidth+user.barPadding))/2+218,canvas.height/2+40+user.headerSize+user.subheaderSize)
}
